#ifndef FRAMEINFO_H
#define FRAMEINFO_H

#include <QObject>
#include <opencv2/opencv.hpp>

struct Pedestrian
{
    int timeAppeared;
    std::deque<cv::Rect> pedestrianRecord;
};

class FrameInfo
{
public:
    FrameInfo();
    FrameInfo(const FrameInfo &other);
    ~FrameInfo();

    std::vector<Pedestrian> pedestriansVector;
};

#endif // FRAMEINFO_H
