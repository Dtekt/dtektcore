/********************************************************************************
** Form generated from reading UI file 'configdialog.ui'
**
** Created: Sun Aug 18 09:49:14 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIGDIALOG_H
#define UI_CONFIGDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGraphicsView>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConfigDialog
{
public:
    QGroupBox *groupBox;
    QCheckBox *virtualFenceCrossingCheckBox;
    QCheckBox *loiteringCheckBox;
    QCheckBox *abandonedItemsCheckBox;
    QCheckBox *suspiciousCrowdActivityCheckBox;
    QGroupBox *groupBox_2;
    QLabel *label;
    QComboBox *alarmTypeComboBox;
    QLabel *label_2;
    QSpinBox *alarmDwellTimeSpinBox;
    QLabel *label_4;
    QGraphicsView *graphicsView;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLineEdit *logEntryFilepathLineEdit;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QGroupBox *groupBox_3;
    QRadioButton *onlineRadioButton;
    QRadioButton *offlineRadioButton;

    void setupUi(QDialog *ConfigDialog)
    {
        if (ConfigDialog->objectName().isEmpty())
            ConfigDialog->setObjectName(QString::fromUtf8("ConfigDialog"));
        ConfigDialog->resize(606, 393);
        QIcon icon;
        icon.addFile(QString::fromUtf8("configure_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        ConfigDialog->setWindowIcon(icon);
        groupBox = new QGroupBox(ConfigDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(390, 90, 181, 141));
        groupBox->setAutoFillBackground(true);
        virtualFenceCrossingCheckBox = new QCheckBox(groupBox);
        virtualFenceCrossingCheckBox->setObjectName(QString::fromUtf8("virtualFenceCrossingCheckBox"));
        virtualFenceCrossingCheckBox->setGeometry(QRect(9, 25, 151, 17));
        loiteringCheckBox = new QCheckBox(groupBox);
        loiteringCheckBox->setObjectName(QString::fromUtf8("loiteringCheckBox"));
        loiteringCheckBox->setGeometry(QRect(9, 52, 151, 17));
        abandonedItemsCheckBox = new QCheckBox(groupBox);
        abandonedItemsCheckBox->setObjectName(QString::fromUtf8("abandonedItemsCheckBox"));
        abandonedItemsCheckBox->setGeometry(QRect(9, 79, 151, 17));
        suspiciousCrowdActivityCheckBox = new QCheckBox(groupBox);
        suspiciousCrowdActivityCheckBox->setObjectName(QString::fromUtf8("suspiciousCrowdActivityCheckBox"));
        suspiciousCrowdActivityCheckBox->setGeometry(QRect(9, 106, 157, 17));
        groupBox_2 = new QGroupBox(ConfigDialog);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(390, 240, 181, 101));
        groupBox_2->setAutoFillBackground(true);
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(11, 21, 53, 16));
        alarmTypeComboBox = new QComboBox(groupBox_2);
        alarmTypeComboBox->setObjectName(QString::fromUtf8("alarmTypeComboBox"));
        alarmTypeComboBox->setGeometry(QRect(89, 21, 56, 20));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(11, 61, 80, 16));
        alarmDwellTimeSpinBox = new QSpinBox(groupBox_2);
        alarmDwellTimeSpinBox->setObjectName(QString::fromUtf8("alarmDwellTimeSpinBox"));
        alarmDwellTimeSpinBox->setGeometry(QRect(97, 61, 33, 20));
        label_4 = new QLabel(ConfigDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(20, 20, 91, 21));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label_4->setFont(font);
        graphicsView = new QGraphicsView(ConfigDialog);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(60, 50, 270, 220));
        layoutWidget = new QWidget(ConfigDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(61, 301, 271, 22));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAutoFillBackground(true);

        horizontalLayout_3->addWidget(label_3);

        logEntryFilepathLineEdit = new QLineEdit(layoutWidget);
        logEntryFilepathLineEdit->setObjectName(QString::fromUtf8("logEntryFilepathLineEdit"));

        horizontalLayout_3->addWidget(logEntryFilepathLineEdit);

        okButton = new QPushButton(ConfigDialog);
        okButton->setObjectName(QString::fromUtf8("okButton"));
        okButton->setGeometry(QRect(400, 360, 75, 23));
        cancelButton = new QPushButton(ConfigDialog);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));
        cancelButton->setGeometry(QRect(490, 360, 75, 23));
        groupBox_3 = new QGroupBox(ConfigDialog);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(390, 30, 181, 51));
        groupBox_3->setAutoFillBackground(true);
        onlineRadioButton = new QRadioButton(groupBox_3);
        onlineRadioButton->setObjectName(QString::fromUtf8("onlineRadioButton"));
        onlineRadioButton->setGeometry(QRect(20, 20, 51, 17));
        offlineRadioButton = new QRadioButton(groupBox_3);
        offlineRadioButton->setObjectName(QString::fromUtf8("offlineRadioButton"));
        offlineRadioButton->setGeometry(QRect(110, 20, 51, 17));

        retranslateUi(ConfigDialog);

        QMetaObject::connectSlotsByName(ConfigDialog);
    } // setupUi

    void retranslateUi(QDialog *ConfigDialog)
    {
        ConfigDialog->setWindowTitle(QApplication::translate("ConfigDialog", "Configure Profile", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("ConfigDialog", "Detection Options", 0, QApplication::UnicodeUTF8));
        virtualFenceCrossingCheckBox->setText(QApplication::translate("ConfigDialog", "Line Crossing Detection", 0, QApplication::UnicodeUTF8));
        loiteringCheckBox->setText(QApplication::translate("ConfigDialog", "Loitering Detection", 0, QApplication::UnicodeUTF8));
        abandonedItemsCheckBox->setText(QApplication::translate("ConfigDialog", "Directional Alarm", 0, QApplication::UnicodeUTF8));
        suspiciousCrowdActivityCheckBox->setText(QApplication::translate("ConfigDialog", "Suspicious Crowd Behaviour", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("ConfigDialog", "Alarm Options", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ConfigDialog", "Alarm Type", 0, QApplication::UnicodeUTF8));
        alarmTypeComboBox->clear();
        alarmTypeComboBox->insertItems(0, QStringList()
         << QApplication::translate("ConfigDialog", "Siren", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ConfigDialog", "Popup", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ConfigDialog", "Visual", 0, QApplication::UnicodeUTF8)
        );
        label_2->setText(QApplication::translate("ConfigDialog", "Alarm Dwell Time", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("ConfigDialog", "New Profile", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ConfigDialog", "Save Log Entries", 0, QApplication::UnicodeUTF8));
        logEntryFilepathLineEdit->setText(QApplication::translate("ConfigDialog", "    C:/Athena/Storage/Log Data/", 0, QApplication::UnicodeUTF8));
        okButton->setText(QApplication::translate("ConfigDialog", "Create", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("ConfigDialog", "Cancel", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("ConfigDialog", "Analysis Type", 0, QApplication::UnicodeUTF8));
        onlineRadioButton->setText(QApplication::translate("ConfigDialog", "Online", 0, QApplication::UnicodeUTF8));
        offlineRadioButton->setText(QApplication::translate("ConfigDialog", "Offline", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ConfigDialog: public Ui_ConfigDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGDIALOG_H
