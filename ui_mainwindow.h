/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sun Aug 18 09:49:13 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionExit;
    QAction *actionEdit_Configuration_Profile;
    QAction *actionAbout_Athena;
    QAction *actionAthena_Help;
    QAction *actionPlugins;
    QAction *actionSave;
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *logLabel;
    QPlainTextEdit *logView;
    QComboBox *configProfiles;
    QTabWidget *tabWidget;
    QWidget *tabOnline;
    QLabel *onlineLabel;
    QWidget *tabOffline;
    QLabel *offlineLabel;
    QPushButton *snapShot;
    QPushButton *pauseOrResumeBtn;
    QSlider *videoSlider;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout;
    QLabel *crowdCountLabel;
    QLCDNumber *crowdCount;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuOptions;
    QMenu *menuHelp;
    QMenu *menuPlugins;
    QStatusBar *statusbar;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1113, 593);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/res/drawables/eye_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setIconSize(QSize(30, 30));
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/res/drawables/open_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon1);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionEdit_Configuration_Profile = new QAction(MainWindow);
        actionEdit_Configuration_Profile->setObjectName(QString::fromUtf8("actionEdit_Configuration_Profile"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/res/drawables/configure_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEdit_Configuration_Profile->setIcon(icon2);
        actionAbout_Athena = new QAction(MainWindow);
        actionAbout_Athena->setObjectName(QString::fromUtf8("actionAbout_Athena"));
        actionAthena_Help = new QAction(MainWindow);
        actionAthena_Help->setObjectName(QString::fromUtf8("actionAthena_Help"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/res/drawables/help_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAthena_Help->setIcon(icon3);
        actionPlugins = new QAction(MainWindow);
        actionPlugins->setObjectName(QString::fromUtf8("actionPlugins"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/res/drawables/save_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon4);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(11, 73, 261, 301));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        logLabel = new QLabel(layoutWidget);
        logLabel->setObjectName(QString::fromUtf8("logLabel"));
        QFont font;
        font.setPointSize(8);
        font.setBold(true);
        font.setWeight(75);
        logLabel->setFont(font);
        logLabel->setAutoFillBackground(true);

        verticalLayout->addWidget(logLabel);

        logView = new QPlainTextEdit(layoutWidget);
        logView->setObjectName(QString::fromUtf8("logView"));
        logView->setReadOnly(true);

        verticalLayout->addWidget(logView);

        configProfiles = new QComboBox(centralWidget);
        configProfiles->setObjectName(QString::fromUtf8("configProfiles"));
        configProfiles->setGeometry(QRect(12, 16, 171, 21));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(300, 10, 781, 441));
        tabOnline = new QWidget();
        tabOnline->setObjectName(QString::fromUtf8("tabOnline"));
        onlineLabel = new QLabel(tabOnline);
        onlineLabel->setObjectName(QString::fromUtf8("onlineLabel"));
        onlineLabel->setGeometry(QRect(0, 0, 776, 417));
        onlineLabel->setAutoFillBackground(true);
        tabWidget->addTab(tabOnline, QString());
        tabOffline = new QWidget();
        tabOffline->setObjectName(QString::fromUtf8("tabOffline"));
        offlineLabel = new QLabel(tabOffline);
        offlineLabel->setObjectName(QString::fromUtf8("offlineLabel"));
        offlineLabel->setGeometry(QRect(0, 0, 776, 417));
        offlineLabel->setAutoFillBackground(true);
        tabWidget->addTab(tabOffline, QString());
        snapShot = new QPushButton(centralWidget);
        snapShot->setObjectName(QString::fromUtf8("snapShot"));
        snapShot->setGeometry(QRect(350, 470, 36, 32));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/res/drawables/snapshot_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        snapShot->setIcon(icon5);
        snapShot->setIconSize(QSize(24, 24));
        pauseOrResumeBtn = new QPushButton(centralWidget);
        pauseOrResumeBtn->setObjectName(QString::fromUtf8("pauseOrResumeBtn"));
        pauseOrResumeBtn->setGeometry(QRect(300, 470, 36, 32));
        QPalette palette;
        QBrush brush(QColor(17, 17, 240, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        pauseOrResumeBtn->setPalette(palette);
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/res/drawables/play_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        pauseOrResumeBtn->setIcon(icon6);
        pauseOrResumeBtn->setIconSize(QSize(24, 24));
        pauseOrResumeBtn->setCheckable(true);
        videoSlider = new QSlider(centralWidget);
        videoSlider->setObjectName(QString::fromUtf8("videoSlider"));
        videoSlider->setGeometry(QRect(410, 470, 451, 32));
        videoSlider->setOrientation(Qt::Horizontal);
        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(895, 471, 185, 31));
        horizontalLayout = new QHBoxLayout(layoutWidget1);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        crowdCountLabel = new QLabel(layoutWidget1);
        crowdCountLabel->setObjectName(QString::fromUtf8("crowdCountLabel"));
        crowdCountLabel->setEnabled(false);
        crowdCountLabel->setAutoFillBackground(true);

        horizontalLayout->addWidget(crowdCountLabel);

        crowdCount = new QLCDNumber(layoutWidget1);
        crowdCount->setObjectName(QString::fromUtf8("crowdCount"));
        crowdCount->setEnabled(false);
        crowdCount->setAutoFillBackground(true);

        horizontalLayout->addWidget(crowdCount);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1113, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuOptions = new QMenu(menuBar);
        menuOptions->setObjectName(QString::fromUtf8("menuOptions"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuPlugins = new QMenu(menuHelp);
        menuPlugins->setObjectName(QString::fromUtf8("menuPlugins"));
        MainWindow->setMenuBar(menuBar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuOptions->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuFile->addAction(actionExit);
        menuOptions->addAction(actionEdit_Configuration_Profile);
        menuHelp->addAction(actionAbout_Athena);
        menuHelp->addAction(actionAthena_Help);
        menuHelp->addAction(menuPlugins->menuAction());
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionOpen);
        mainToolBar->addAction(actionSave);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionEdit_Configuration_Profile);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionAthena_Help);

        retranslateUi(MainWindow);
        QObject::connect(actionExit, SIGNAL(triggered()), MainWindow, SLOT(close()));

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "DTecht Video Analyzer", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "Open...", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "Quit", 0, QApplication::UnicodeUTF8));
        actionEdit_Configuration_Profile->setText(QApplication::translate("MainWindow", "Edit Configuration Profile...", 0, QApplication::UnicodeUTF8));
        actionAbout_Athena->setText(QApplication::translate("MainWindow", "About Athena...", 0, QApplication::UnicodeUTF8));
        actionAthena_Help->setText(QApplication::translate("MainWindow", "Athena Help...", 0, QApplication::UnicodeUTF8));
        actionPlugins->setText(QApplication::translate("MainWindow", "Plugins...", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("MainWindow", "Save Log", 0, QApplication::UnicodeUTF8));
        logLabel->setText(QApplication::translate("MainWindow", " Event Log", 0, QApplication::UnicodeUTF8));
        configProfiles->clear();
        configProfiles->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "CCTV Camera 1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "CCTV Camera 2", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "CCTV Camera 3", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "CCTV Camera 4", 0, QApplication::UnicodeUTF8)
        );
        onlineLabel->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tabOnline), QApplication::translate("MainWindow", "Online", 0, QApplication::UnicodeUTF8));
        offlineLabel->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tabOffline), QApplication::translate("MainWindow", "Offline", 0, QApplication::UnicodeUTF8));
        snapShot->setText(QString());
        pauseOrResumeBtn->setText(QString());
        crowdCountLabel->setText(QApplication::translate("MainWindow", "Estimated Crowd Count", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuOptions->setTitle(QApplication::translate("MainWindow", "Options", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
        menuPlugins->setTitle(QApplication::translate("MainWindow", "Plugins..", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
