#include "mainwindow.h"
#include "plugininterface.h"
#include "ui_mainwindow.h"
#include "configdialog.h"

#include <QPluginLoader>
#include <QFileDialog>
#include <QDateTime>
#include <QSize>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    playIcon.addFile(":/res/drawables/play_icon.png", QSize(24,24));
    pauseIcon.addFile(":/res/drawables/pause_icon.png", QSize(24,24));
    frameCount = 0;

    singleplugindemo = new QObject(); //plugin demo
    videoFileName = "C:/Users/ANURUDDHA/pedestrians/ThreePastShop2cor.mpg"; //for fast testing

    processObj.vcapInput = &vcap;
    processObj.lblOutput = ui->offlineLabel;    // unnecessary if only main thread is allowed to update UI

    connect(&processorThread, SIGNAL(started()), &processObj, SLOT(processFrame()));
    processObj.moveToThread(&processorThread);
    connect(&processObj, SIGNAL(frameReady(FrameInfo *, QImage *)), this, SLOT(displayFrame(FrameInfo *, QImage *)));
    ui->setupUi(this);
    loadPlugins();
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::loadPlugins()
{
    pluginsDir = QDir(qApp->applicationDirPath());
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release") {
        pluginsDir.cdUp();
    }
    pluginsDir.cd("plugins"); //either absolute path
    pluginList.clear();

    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *pluginObject = loader.instance();
        if (pluginObject) {
            PluginInterface *plugin = qobject_cast<PluginInterface *>(pluginObject);
            if(plugin) {
//                pluginList += pluginnew;
                singleplugindemo = pluginObject;  //demo
                plugin->prepareGUI(this);
            }
        }
    }
    if(pluginList.empty()){
        return false;
    } else {
        return true;
    }
}

void MainWindow::displayFrame(FrameInfo *frameInfo , QImage *image)
{
//    ui->offlineLabel->setPixmap(QPixmap::fromImage(image->scaledToHeight(415,Qt::SmoothTransformation)));
    frameCount++;
    PluginInterface *plugin = qobject_cast<PluginInterface *>(singleplugindemo); //demo getting plugins from list //foreach plugin
    if(plugin){
        plugin->detectAndDisplay(this, frameInfo);
        ui->offlineLabel->setText("Frame no - " + QString::number(frameCount));
    }else{
        ui->offlineLabel->setText("No plugin..");
    }
}

void MainWindow::on_actionEdit_Configuration_Profile_triggered()
{
    ConfigDialog configDialog;
    configDialog.exec();
}

void MainWindow::on_actionOpen_triggered()
{
    QFileDialog fileDialog(this);
    fileDialog.setNameFilter(tr("Videos (*.flv *.mp4 *.avc *.mpg)"));
    fileDialog.setViewMode(QFileDialog::Detail);
    videoFileName = fileDialog.getOpenFileName(this, tr("Select Video File"), "C:/");
    if(!videoFileName.isEmpty()){
        updateLog(videoFileName + " has been loaded..");
//        processorThread.wait(); /////////////////////////////////////////////stop thread
    }
}

void MainWindow::updateLog(QString logVal)
{
    ui->logView->appendPlainText(logVal + "\n" + QDateTime::currentDateTime().toString() + "\n");
}

void MainWindow::on_tabWidget_selected(const QString &tabLable)
{
    updateLog(tabLable + " mode selected..");
    if(tabLable.operator ==("Offline")){
        ui->videoSlider->setEnabled(true);
    } else if (tabLable.operator ==("Online")){
        ui->videoSlider->setEnabled(false);
    }
    ui->pauseOrResumeBtn->setChecked(false);
}

void MainWindow::on_pauseOrResumeBtn_toggled(bool checked)
{
    if(checked){
        if(videoFileName.isEmpty()){
            on_actionOpen_triggered();
        }
        if(!videoFileName.isEmpty()){
            if(!processorThread.isRunning()){
                ui->pauseOrResumeBtn->setIcon(pauseIcon);
                updateLog("Detection started..");
                vcap.open(videoFileName.toStdString());
                vcap.set(CV_CAP_PROP_FPS, 25);
                processorThread.start(QThread::HighPriority);
            }
        } else {
            ui->pauseOrResumeBtn->setChecked(false);
        }
    }else{
        ui->pauseOrResumeBtn->setIcon(playIcon);
        updateLog("Detection suspended..");
        processorThread.terminate();
        if(processorThread.isRunning()){            //debugginh
            updateLog("But Thread is running..");
        }
    }
}
