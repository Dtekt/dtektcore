#include "processor.h"
#include <QThread>
#include <QMutex>
#include <QImage>

ProcessorObj::ProcessorObj(QObject *parent) : QObject(parent)
{
    matInput = new cv::Mat();
    PINK = cv::Scalar(255, 0, 255);
    BLUE = cv::Scalar(255, 0, 0);
    GOLD = cv::Scalar(0,215,255);
    RED = cv::Scalar(0,0,255);
    global_ID = 1;
}

void ProcessorObj::processFrame()
{
    int frameNo = 0;
    if(vcapInput->isOpened()){
        while(vcapInput->read(*matInput)){
            if(!matInput->empty()){
                frameNo++;
//                cv::resize(*matInput, *matInput, cv::Size(), 2.5, 2.5);
                detect(*matInput);
                track(*matInput);
                combine(*matInput);

                cv::cvtColor(*matInput, *matInput, CV_BGR2RGB);
                QImage qimgOffline((uchar*)matInput->data, matInput->cols, matInput->rows, matInput->step, QImage::Format_RGB888);

//                lblOutput->setText(QString::number(previous_pedestrians.size()));

                FrameInfo *frameinfo = new FrameInfo();
                foreach (a_pedestrian i, previous_pedestrians) {
                    std::deque<cv::Rect> dummyDeque;    //to be removed
                    dummyDeque.push_back(i.rect);    //to be removed

                    Pedestrian pedestrian;
                    pedestrian.timeAppeared = 1234;    //  i.timeAppeared
                    pedestrian.pedestrianRecord = dummyDeque;   //i.pedestrianRecord
                    frameinfo->pedestriansVector.push_back(pedestrian);

                }
                emit frameReady(frameinfo, &qimgOffline);
            }
        }
    }
}

void ProcessorObj::detect(cv::Mat matToDetect)
{
    std::vector<cv::Rect> found; // Will store the array of Rectangles containing the detected people
//	 temp_image = image.clone();

    cv::HOGDescriptor hog;
    hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());

    hog.detectMultiScale(matToDetect, found, 0.5, cv::Size(8,8), cv::Size(0,0), 1.05, 1);

    size_t i, j;

//     First we have to remove squares inside another one. If there is a square inside another it is rejected and
//     remaining squares are entered to the 'found_filtered' vector

    for( i = 0; i < found.size(); i++ )
    {
        cv::Rect r = found[i];
        for( j = 0; j < found.size(); j++ )
            if( j != i && (r & found[j]) == r)
                break;
        if( j == found.size() )
                detected_pedestrians.push_back(r);
    }

    // Now the filtered squares are drawn on image - those will be showing the people detected

    for( i = 0; i < detected_pedestrians.size(); i++ )
    {
        cv::Rect r = detected_pedestrians[i];
        // the HOG detector returns slightly larger rectangles than the real objects.
        // so we slightly shrink the rectangles to get a nicer output.
        r.x += cvRound(r.width*0.1);
        r.width = cvRound(r.width*0.8);
        r.y += cvRound(r.height*0.07);
        r.height = cvRound(r.height*0.8);

        detected_pedestrians[i] = r;
        // rectangle(temp_image, r.tl(), r.br(), GREEN , 3);
    }
}

void ProcessorObj::track(cv::Mat matToTrack)
{
    if(previous_pedestrians.size() != 0){
        //cout << "1";
        for(int i = 0; i < previous_pedestrians.size(); i++ )
        {
            cv::Rect r = previous_pedestrians[i].rect;
            // Four if conditions are used to avoid the errors given during the cropping.
            // If the rectangle coordinates are out of the image, cropping will give an error

            r.x += cvRound(r.width*0.2);
            r.width = cvRound(r.width*0.7);
            r.y += cvRound(r.height*0.17);
            r.height = cvRound(r.height*0.7);

            if (r.x < 0 )
                r.x = 0;
            if (r.y < 0)
                r.y = 0;
            if (r.br().x > matToTrack.cols)
                r.width = matToTrack.cols - r.x ;
            if (r.br().y > matToTrack.rows)
                r.height = matToTrack.rows - r.y ;


            cv::Mat person = previous_image(r);				// Crop the rectangle area that contains the person

            cv::Point pt;
            cv::Mat gray_image;				// Will store the gray scale image
            cv::Mat points_2_track;


            //resize(image, image, Size(floor(SCALE*image.cols),floor(SCALE*image.rows)));		// Image Resized

            cv::cvtColor(person, gray_image, CV_RGB2GRAY);										// Converted to Gray scale
            cv::goodFeaturesToTrack(gray_image,points_2_track, MAX_FEATURES, 0.1, MIN_DISTANCE);		// Feature Detection

            /*
            // Marking the good features on the image
            for (int i = 0 ; i < points_2_track.rows ; i++){
                try {
                    pt.x = points_2_track.at<float>(i,0);
                    pt.y = points_2_track.at<float>(i,1);
                    circle(person,pt,RADIUS,GREEN);
                }
                catch (...) {
                    cout << "ERROR inside good_features_to_track";
                    break;
                }
            }

            char numstr[21]; // enough to hold all numbers up to 64-bits
            sprintf(numstr, "%d", i);
            //destroyWindow(numstr);	// If there is a previous window open, it'll be closed
            imshow(numstr,person);
            waitKey(1);			// This make sure the image is displayed. If not you won't get an image.
            */
            // ********************************************************************************

            // Features are detected in the cropped square. Now we have to transfer the cordinates from that
            // small cropped image to the large frame cordinates and mark them on that image ('image')
            // And also calculate the average point of all the features

            int number_of_features_detected = points_2_track.rows;
            cv::Point pt_onPerson;

            int avg_x = 0;
            int avg_y= 0;

            // Draws selected features on the image ( complete image)
            for (int j = 0 ; j < number_of_features_detected ; j++){
                try {

                    // Transferring coordinates from the cropped image ('person') to the large image('image') coordinate system
                    points_2_track.at<float>(j,0)  += r.x;
                    points_2_track.at<float>(j,1)  += r.y;

                    // Marking the detected features on 'image'
                    pt_onPerson.x = points_2_track.at<float>(j,0);
                    pt_onPerson.y = points_2_track.at<float>(j,1);
                    //circle(temp_image,pt_onPerson,RADIUS,GREEN);

                    // Calculates the average of the detected features
                    avg_x += pt_onPerson.x;
                    avg_y += pt_onPerson.y;

                }
                catch (...) {
//                    std::cout << "ERROR after calling good_features_to_track";
                    break;
                }
            }

            // Calculate the average point of all the features. Here after I'll be
            // addressing it as the PINK point. It'll be displayed as a PINK circle on the image.

            avg_x = avg_x/points_2_track.rows;
            avg_y = avg_y/points_2_track.rows;

            // Display the PINK circle
            cv::circle(temp_image,cv::Point(avg_x,avg_y),PINK_RADIUS,PINK);

            previous_pink_point  = cv::Point(avg_x,avg_y);
            // Used in tracking. To track you need to know where was the initial PINK point was.
            // this method always gives the initial set of points for the tracking.

            cv::Mat status;
            cv::Mat err;
            cv::Point pt_onPerson_tracked;
            cv::Point prev_pt_onPerson_tracked;
            cv::Mat current_points;

            // Calculate the optical flow
            cv::calcOpticalFlowPyrLK(previous_image, matToTrack, points_2_track, current_points, status, err);

            // Marking the tracked good features on the image
           avg_x = 0; avg_y= 0 ;
           int count = 0;

            for (int k = 0 ; k < current_points.rows ; k++){
                try {

                    if ( err.at<float>(k,0) < 30 && err.at<float>(k,0) > 0 && (int)status.at<char>(k,0) == 1){
                                                                                                 // Checks the error of the ith feature. Display it only if the
                        // error is small
                        // Mark the tracked point
                        pt_onPerson_tracked.x = current_points.at<float>(k,0);
                        pt_onPerson_tracked.y = current_points.at<float>(k,1);

                        /*prev_pt_onPerson_tracked.x = points_2_track.at<float>(k,0);
                        prev_pt_onPerson_tracked.y = points_2_track.at<float>(k,1);

                        Point moviement = pt_onPerson_tracked - prev_pt_onPerson_tracked;
                        float distance = sqrt(pow((float)moviement.x,2) + pow((float)moviement.y,2));
                        cout << "distance : " << distance << "\n";*/

                        cv::circle(temp_image,pt_onPerson_tracked,RADIUS,BLUE);

                        count++;

                        // Calculating the PINK point for the current frame
                        avg_x += pt_onPerson_tracked.x;
                        avg_y += pt_onPerson_tracked.y;
                    }
                }
                catch (...) {
//                    std::cout << "ERROR after calcOpticalFlowPyrLK !!!";
                    break;
                }

            }

            // This will give an indication about the number of features we have lost track of.
            // If it is 0 all the previous_points are tracked with error less than 30
            // If it is 1 all the previous_points are lost
            float tracked_to_prev_point_ratio = ((float)(number_of_features_detected   - count))/number_of_features_detected;

            // If tracked_to_prev_point_ratio is < 0.6 we have enough amount of features being track to calculate the position
            // of the person in the current frame. So we do the updating of the rectangle.
            if (tracked_to_prev_point_ratio < 0.7 ){
                avg_x = avg_x/count;
                avg_y = avg_y/count;

                // Calculate the current PINK point
                cv::Point current_pink_point = cv::Point(avg_x,avg_y);

                // Calculate the movement of the PINK point and transfer the rectangle from the previous frame by that amount
                cv::Rect current_rect = previous_pedestrians[i].rect + (current_pink_point - previous_pink_point);

                // Mark the rectangle on the image
                // rectangle(temp_image, current_rect.tl(), current_rect.br(), BLUE , 3);  // (r.x,r.y) are the coordinates of the top left corner of the Rect


                // Mark the PINK point
                cv::circle(matToTrack,cv::Point(avg_x,avg_y),PINK_RADIUS,PINK);

                a_pedestrian temp_tracked;
                temp_tracked.rect = current_rect;
                temp_tracked.ID = previous_pedestrians[i].ID;
                tracked_pedestrians.push_back(temp_tracked);

            }
        }
    }
    //else cout << "2";
}

void ProcessorObj::combine(cv::Mat matToCombine)
{
    previous_image = matToCombine.clone();

    float avg_height = 0;
    float avg_width = 0;

    int* detected_marked;
    detected_marked = new int[detected_pedestrians.size()];

    int* tracked_marked;
    tracked_marked = new int[tracked_pedestrians.size()];

    for (int i = 0; i<tracked_pedestrians.size(); i++)
        tracked_marked[i] = 0;		// Initialize the array to zero

    // It should be the finalized pedestrian list.

    for (int i = 0; i < detected_pedestrians.size() ; i++ ){  // Goes through the detected pedestrians in the current frame


        cv::Rect detected = detected_pedestrians[i];
//        rectangle(temp_image,detected.tl(), detected.br(), BLUE , 3);   // FOR DEBUGGING*****************

        float min_distance = 1000;
        int matched_ID = -1;
        int matched_index = -1;

        //Mat temp_temp = temp_image.clone();                   // FOR DEBUGGING*****************

        for (int j = 0; j < tracked_pedestrians.size() ; j++){ // Now for a given detection, we sweep through all the tracked pedestrians to see whether
                                                               // there is a good matching tracked pedestrian.

            cv::Rect tracked = tracked_pedestrians[j].rect;
            float dist = distanceBetweenTwoRects(detected,tracked);       // Checks the distance between detected rect and tracked rect
            //cout << "a "<< j << "\n";
            //rectangle(temp_temp,tracked.tl(), tracked.br(), PURPLE , 3);     // FOR DEBUGGING*****************

            char numstr[100]; // enough to hold all numbers up to 64-bits
//            std::sprintf(numstr, "%.3f", dist);

            //putText(temp_temp,numstr , Point(tracked.br().x - tracked.width,tracked.br().y - tracked.height - 10), FONT_HERSHEY_COMPLEX_SMALL , 0.5 ,PURPLE );  // FOR DEBUGGING*****************

            if (dist < min_distance){       // Finds the minimum cost
                min_distance = dist;
                matched_ID = tracked_pedestrians[j].ID;
                matched_index = j;
            }
        }

        // ********************* TODO ***************************
        // Have to handle the case where you get equal distances to two tracked rectangles !!!
        // ******************************************************



        if (min_distance < 30)  // This value 30 is stupid value. This needs when people walk close by. this has to be determined using the Probabilistic model
                                // we are going to introduce later. till then lets keep it as a constant.
        {
            for (int k = 0; k < detected_pedestrians.size(); k++){   // This block is to handle a special case.
            // Special Case : For a new pedestrian, he is not tracked previously. So if there is someone else who is tracked previously
            // close to this person with a distance less than VALUE IN BELOW IF CONDITION ( 31 one at the moment ), that tracked
            // ID will be assigned to him - THAT IS SO WRONG. SOMEONE ELSE ID IS ASSIGNED TO HIM.
            // SO if we get a match, we check whether there are any other detected rectangles which are more close to this tracked_rect;

            // WARNING : THERE IS A SCENEARIO IN WHICH THIS WOULD FAIL. SAY THERE ARE TWO PEOPLE - 1 AND 2 WALKING VERY CLOSE. 1 IS DETECTED IN THE 10TH FRAME AND 2 IS NOT.
            // 2 IS DETECTED FOR THE FIRST TIME IN THE 11TH FRAME BUT 1 IS MISSED IN THE 11TH FRAME. NOW FOR 2 THERE WILL BE A TRACKED FRAME CLOSE BY (IT'S 1s TRACKED RECT)
            // SO IT COMES HERE TO CHECK WHETHER IT BELONGS TO SOMEONE ELSE. BUT CAN'T FIND IT BECAUSE 1 IS NOT IN THE DETECTED_PEDESTRIAN VECTOR. BINGO. IT FAILS !!!
                if (k != i){
                    float dist = distanceBetweenTwoRects(detected_pedestrians[k],tracked_pedestrians[matched_index].rect);
                    if (dist < min_distance)
                        min_distance = 1000;
                }

            }
        }


        // At this point we now whether the ith pedestrian in the detected_pedestrian vector has a matching tracked rectangle. So if he has it'll
        // goes in to the 'if' and if not it'll goes in to the 'else'

        if (min_distance != 1000 && min_distance < 30){  // GOLDEN. The detected pedestrian also has been tracked. That means, we know him already.
                                                         // Get the ID from matched_ID and put in to the 'pedestrians' vector.
            a_pedestrian temp_tracked;
            temp_tracked.rect = detected;
            temp_tracked.ID = matched_ID;
            pedestrians.push_back(temp_tracked);

            tracked_marked[matched_index] = 1;

            cv::rectangle(temp_image, detected.tl(), detected.br(), GOLD, 3); //*****************
            char numstr[21]; // enough to hold all numbers up to 64-bits
//            std::sprintf(numstr, "%d", matched_ID);
            cv::putText(temp_image, numstr, cv::Point(detected.br().x - detected.width, detected.br().y + 15), cv::FONT_HERSHEY_COMPLEX_SMALL , 0.7, GOLD);


        } else {                                  // SILVER. Seems we have a new comer. Give him a new ID and put him to the 'pedestrians' vector

            cv::rectangle(temp_image, detected.tl(), detected.br(), BLUE, 3);   // *****************

            a_pedestrian temp_pedestrian;
            temp_pedestrian.rect = detected;
            temp_pedestrian.ID = global_ID;
            pedestrians.push_back(temp_pedestrian);

            //detected_marked[i] = 1;

            char numstr[21]; // enough to hold all numbers up to 64-bits
//            std::sprintf(numstr, "%d", global_ID);
            cv::putText(temp_image,numstr , cv::Point(detected.br().x - detected.width,detected.br().y + 15), cv::FONT_HERSHEY_COMPLEX_SMALL , 0.7, BLUE);

            global_ID++;


        }
        //imshow("temp",temp_temp);  // FOR DEBUGGING *****************
        //cout << "i :" << i << " Detected count : " << detected_pedestrians.size() << "\n";
        // waitKey(0);                    // FOR DEBUGGING *****************

    }

    for (int i = 0; i < tracked_pedestrians.size(); i++){  // RED. Detector failures. Previously there, but not detected this time. So we'll mark them in RED and
                                                           // put them in to the 'pedestrian' vector expecting the detector failed not because he left it
        // ************************ TODO **********************************
        // Make sure he has not left the scene before entering him to the 'pedestrian list'. The failure of the detector might be due to this fact.
        // ****************************************************************

        if (tracked_marked[i] == 0){
            cv::rectangle(temp_image, tracked_pedestrians[i].rect.tl(), tracked_pedestrians[i].rect.br(), RED, 3);

            char numstr[21]; // enough to hold all numbers up to 64-bits
//            std::sprintf(numstr, "%d", tracked_pedestrians[i].ID);
            cv::putText(temp_image, numstr, cv::Point(tracked_pedestrians[i].rect.br().x - tracked_pedestrians[i].rect.width, tracked_pedestrians[i].rect.br().y + 15), cv::FONT_HERSHEY_COMPLEX_SMALL , 0.7, RED);

            a_pedestrian temp_tracked;
            temp_tracked.rect = tracked_pedestrians[i].rect;
            temp_tracked.ID = tracked_pedestrians[i].ID;
            pedestrians.push_back(temp_tracked);

            tracked_marked[i] = 1;

        }
    }

    //for (int i = 0; i < detected_pedestrians.size(); i++){
    //
    //	if (detected_marked[i] == 0 ){
    //		rectangle(temp_image, detected_pedestrians[i].tl(), detected_pedestrians[i].br(), BLUE , 3);

    //		a_pedestrian temp_pedestrian;
    //		temp_pedestrian.rect = detected_pedestrians[i];
    //		temp_pedestrian.ID = global_ID;
    //		pedestrians.push_back(temp_pedestrian);

    //		detected_marked[i] = 1;

    //		char numstr[21]; // enough to hold all numbers up to 64-bits
    //		sprintf(numstr, "%d", global_ID);
    //		putText(temp_image,numstr , Point(detected_pedestrians[i].br().x - detected_pedestrians[i].width,detected_pedestrians[i].br().y + 15), FONT_HERSHEY_COMPLEX_SMALL , 0.7 , BLUE);

    //		global_ID++;

    //	}
    //}

    /*for (int i = 0; i < detected_pedestrians.size(); i++)
        cout << "\nDetected marked : " << detected_marked[i];

    for (int i = 0; i < tracked_pedestrians.size(); i++)
        cout << "\nTracked marked : " << tracked_marked[i];*/

//    std::cout << "\n----------------------------\n";

    delete [] detected_marked;
    delete [] tracked_marked;

    // This is where we have to call for plug ins. Frame is being processed and the locations are extracted.
    previous_pedestrians.clear();
    previous_pedestrians = pedestrians;
    detected_pedestrians.clear();
    tracked_pedestrians.clear();
    pedestrians.clear();

//    cv::imshow("output",temp_image);
}

float ProcessorObj::distanceBetweenTwoRects(cv::Rect rect1, cv::Rect rect2)
{
    cv::Point rect1_middle_point = rect1.tl() + rect1.br();
    rect1_middle_point.x = rect1_middle_point.x/2;
    rect1_middle_point.y = rect1_middle_point.y/2;

    cv::Point rect2_middle_point = rect2.tl() + rect2.br();
    rect2_middle_point.x = rect2_middle_point.x/2;
    rect2_middle_point.y = rect2_middle_point.y/2;

    cv::Point movement = rect1_middle_point-rect2_middle_point;
    float distance = sqrt(pow((float)movement.x,2) + pow((float)movement.y,2));

    return distance;
}

