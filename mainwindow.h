#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//#include "plugininterface.h"  //demo

#include <QDir>
#include <QMainWindow>
#include <QList>
#include <QStack>
#include <QIcon>
#include <QThread>

#include "processor.h"
#include "frameinfo.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void displayFrame(FrameInfo *frameInfo, QImage *image);  //QImage to be removed if worker thread can update UI
    void on_actionEdit_Configuration_Profile_triggered();
    void on_actionOpen_triggered();
    void on_tabWidget_selected(const QString &arg1);
    void on_pauseOrResumeBtn_toggled(bool checked);

private:
    bool loadPlugins();
    void updateLog(QString);

    Ui::MainWindow *ui;
    QDir pluginsDir;
    QList<QObject> pluginList;
    QString videoFileName;
    cv::VideoCapture vcap;
    QIcon playIcon;
    QIcon pauseIcon;

    int frameCount;
    QObject *singleplugindemo; // demo

    ProcessorObj processObj;
    QThread processorThread;
};

#endif
