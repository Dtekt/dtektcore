#ifndef PROCESSOR_H
#define PROCESSOR_H

#define MAX_FEATURES 10
#define MIN_DISTANCE 3
#define PINK_RADIUS 4
#define RADIUS 2

#include <QObject>
#include <QLabel>

//#include <stdio.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "frameinfo.h"

struct a_pedestrian {
    cv::Rect rect;
    int ID;
    int timeAppeared; // appeared time in msecs
    bool isAlive;
    std::deque<cv::Rect> pedestrianRecord;
};

class ProcessorObj : public QObject
{
    Q_OBJECT
public:
    explicit ProcessorObj(QObject *parent = 0);

    bool sleep;
    cv::VideoCapture *vcapInput;
    cv::Mat *matInput;
    QLabel *lblOutput;
    
signals:
    void frameReady(FrameInfo*, QImage*);
    
public slots:
    void processFrame();

private:
    void detect(cv::Mat matToDetect);
    void track(cv::Mat matToTrack);
    void combine(cv::Mat matToCombine);
    float distanceBetweenTwoRects(cv::Rect rect1,cv::Rect rect2);

    std::vector<cv::Rect> detected_pedestrians;
    std::vector<a_pedestrian> tracked_pedestrians;
    std::vector<a_pedestrian> previous_pedestrians;
    std::vector<a_pedestrian> pedestrians;

    cv::Point previous_pink_point;

    cv::Mat previous_image;
    cv::Mat temp_image;

    cv::Scalar PINK;
    cv::Scalar BLUE;
    cv::Scalar GOLD;
    cv::Scalar RED;

    int global_ID;
};

#endif // PROCESSOR_H
