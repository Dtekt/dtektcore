#include "mainwindow.h"
#include "frameinfo.h"

#include <QApplication>
#include <QMetaType>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qRegisterMetaType<FrameInfo>("FrameInfo");
    MainWindow window;
    window.show();
    
    return app.exec();
}
